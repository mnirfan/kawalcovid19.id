import * as React from 'react';
import { NextPage, GetStaticProps, GetStaticPaths } from 'next';
import styled from '@emotion/styled';
import convert from 'htmr';

import { wp } from 'utils/api';
import { WordPressPost } from 'types/wp';
import { PageWrapper, Content, Column } from 'components/layout';
import { Box } from 'components/design-system';
import ErrorPage from 'pages/_error';
import { PostHeader } from 'modules/posts-index';
import htmrTransform from 'modules/posts-index/utils/htmrTransform';

interface PagesContentPageProps {
  post?: WordPressPost;
  errors?: string;
}

const ContentAsSection = Content.withComponent('section');

const Section = styled(ContentAsSection)`
  padding-bottom: 48px;
`;

const PagesContentPage: NextPage<PagesContentPageProps> = ({ post }) => {
  if (!post) {
    return <ErrorPage statusCode={404} />;
  }

  return (
    <PageWrapper title={`${post.title.rendered} | KawalCOVID19`}>
      <Box as="article" display="flex" flexDirection="column" flex="1 1 auto">
        <PostHeader
          type={post.type}
          title={post.title.rendered}
          description={post.excerpt.rendered}
        />
        <Section>
          <Column size="md">
            {convert(post.content.rendered, {
              transform: htmrTransform,
            })}
          </Column>
        </Section>
      </Box>
    </PageWrapper>
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  try {
    if (params) {
      const { paths } = params;
      const post = await wp<WordPressPost>(`wp/v2/pages/${paths[0]}`);

      if (post && post.id) {
        const props = { post } as PagesContentPageProps;
        return { props };
      }
    }

    throw new Error('Failed to fetch page');
  } catch (err) {
    const props = { errors: err.message } as PagesContentPageProps;
    return { props };
  }
};

export const getStaticPaths: GetStaticPaths = async () => {
  const pagesList = await wp<WordPressPost[]>('wp/v2/pages', {
    per_page: 100,
  });

  const paths =
    pagesList && pagesList.length
      ? pagesList.map(page => {
          return { params: { paths: [`${page.id}`, `${page.slug}`] } };
        })
      : [];

  return {
    fallback: true,
    paths,
  };
};

export default PagesContentPage;
